class Persona {

    private nombre : string;
    private apellido : string;

    constructor(nombre : string, apellido : string) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    getNombre() {
        return this.nombre;
    }

    getApellido() {
        return this.apellido;
    }

}

var mario = new Persona("Mario", "Peñate");

document.body.innerHTML = `Mi nombre es ${mario.getNombre()} ${mario.getApellido()}`;