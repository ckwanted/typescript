var Persona = (function () {
    function Persona(nombre, apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }
    Persona.prototype.getNombre = function () {
        return this.nombre;
    };
    Persona.prototype.getApellido = function () {
        return this.apellido;
    };
    return Persona;
}());
var mario = new Persona("Mario", "Peñate");
document.body.innerHTML = "Mi nombre es " + mario.getNombre() + " " + mario.getApellido();
