interface Persona {
    nombre   : string;
    apellido : string;
}

function hola(persona: Persona) {
    return "Hola " + persona.nombre + " " + persona.apellido;
}

var mario = {nombre:"Mario", apellido:"Peñate"};

document.body.innerHTML = hola(mario);  